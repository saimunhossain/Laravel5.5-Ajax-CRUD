<div class="container">
    <div class="row">
        <div class="col-md-7">
            <h3>Simple Ajax CRUD</h3>
        </div>
        <div class="col-md-5">
            <div class="pull-right">
              <div class="input-group">
                <input type="text" 
                        class="form-control"
                        value="{{ request()->session()->get('search') }}"
                        onkeydown="if (event.keyCode == 13) ajaxLoad('{{ url('posts') }}?search'+this.value)"
                        id="search"
                        name="search"
                        placeholder="Search Title">
                <div class="input-group-btn">
                    <button type="submit" name="button"
                     onClick="ajaxLoad('{{ url('posts') }}?search='+$('#search').val())"
                    class="btn btn-primary">
                        <i class="class fa fa-search"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th width="60px">No</th>
            <th>Title Posts</th>
            <th>Description</th>
            <th>
                <a href="javascript:ajaxLoad('{{ url('posts/create') }}')" class="btn btn-xs btn-primary"><i class="fa fa-plus"> New Post</i></a>
            </th>
        </tr>
    </thead>
    <tbody>
        @php
            $i = 1;
        @endphp
        @foreach($posts as $key => $value)
        <tr>
            <td>{{ $i++ }}</td>
            <td>{{ $value->title }}</td>
            <td>{{ $value->description }}</td>
            <td>{{ $value->created_at }}</td>
            <td>{{ $value->updated_at }}</td>
            <td>
                <a href="javascript:ajaxLoad('{{ url('posts/show/'.$value->id) }}')" class="btn btn-xs btn-info">Show</a>
                <a href="javascript:ajaxLoad('{{ url('posts/update/'.$value->id) }}')" class="btn btn-xs btn-info">Edit</a>
                <input type="hidden" name="_method" value="delete"/>
                <a href="javascript:if(confirm('Are you sure want to delete?')) ajaxDelete('{{url('posts/delete/'.$post->id)}}','{{csrf_token()}}')" class="btn btn-xs btn-info">Delete</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<ul class="pagination">
    {{ $posts->links() }}
</ul>
</div>